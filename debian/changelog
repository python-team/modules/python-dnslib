python-dnslib (0.9.25-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.7.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Fri, 12 Jul 2024 09:24:03 -0400

python-dnslib (0.9.24-1) unstable; urgency=medium

  * Add d/source/options extend-diff-ignore to fix dpkg-source failure due to
    local changes (python package metadata regeneration) (Closes: #1046033)
  * Add Rules-Requires-Root: no to d/control
  * Bump standards-version to 4.6.2 without further change
  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Thu, 04 Jan 2024 18:20:23 -0500

python-dnslib (0.9.23-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Tue, 01 Nov 2022 15:38:25 -0400

python-dnslib (0.9.22-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.6.1 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sat, 15 Oct 2022 20:06:42 -0400

python-dnslib (0.9.19-1) unstable; urgency=medium

  * New upstream release
  * Update debian/watch to version 4

 -- Scott Kitterman <scott@kitterman.com>  Thu, 20 Jan 2022 10:09:21 -0500

python-dnslib (0.9.18-1) unstable; urgency=medium

  * Remove d/patches/0001-Only-run-tests-for-python3.patch and set test
    versions in d/rules instead
  * Iterate over supported python3 versions during build test
  * Update dh_auto_test override to check DEB_BUILD_OPTIONS
  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Wed, 12 Jan 2022 10:19:36 -0500

python-dnslib (0.9.16-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
    + Drop check for DEB_BUILD_OPTIONS containing "nocheck", since debhelper now
      does this.

  [ Scott Kitterman ]
  * Update packaging branch from master to debian/master and add debian/
    gbp.conf
  * New upstream release
  * Bump standards-version to 4.6.0 without further change
  * Update autopkgtest to run for all supported python versions

 -- Scott Kitterman <scott@kitterman.com>  Sun, 05 Dec 2021 15:05:46 -0500

python-dnslib (0.9.14-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Wed, 10 Jun 2020 00:51:44 -0400

python-dnslib (0.9.13-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Scott Kitterman ]
  * New upstream release
  * Bump standards-version to 4.5.0 without further change
  * Add Repository and Repository-Browse to upstream metadata

 -- Scott Kitterman <scott@kitterman.com>  Tue, 02 Jun 2020 08:38:45 -0400

python-dnslib (0.9.12-1) unstable; urgency=medium

  * New upstream release
  * d/control: Update Homepage field for move to GitHub
  * d/rules: Honor DEB_BUILD_OPTIONS nocheck
  * d/control: Bump standards-version to 4.4.1 without further change
  * d/control: Bump debhelper-compat from 9 to 12
  * Add d/patches/0001-Only-run-tests-for-python3.patch so tests aren't run
    for python2 (which isn't installed)

 -- Scott Kitterman <scott@kitterman.com>  Mon, 06 Jan 2020 02:41:20 -0500

python-dnslib (0.9.10-1) unstable; urgency=medium

  * New upstream release
  * Add autopkgtest

 -- Scott Kitterman <scott@kitterman.com>  Thu, 01 Aug 2019 22:14:18 -0400

python-dnslib (0.9.7+hg20170303-2) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Thu, 25 Jul 2019 14:51:55 +0200

python-dnslib (0.9.7+hg20170303-1) unstable; urgency=medium

  * Initial release. (Closes: #856672)

 -- Scott Kitterman <scott@kitterman.com>  Fri, 03 Mar 2017 12:37:55 -0500
